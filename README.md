# Workspace

List of things to download to setup workspace (or new computer)

## Browsers

- [Chrome](https://www.google.com/chrome/)
- [Canary Chrome](https://www.google.com/intl/en_sg/chrome/canary/)
- [Firefox](https://www.mozilla.org/en-US/firefox/new/)
- [Brave](https://brave.com/)

## Run-times

- [Python](https://www.python.org/downloads/)
- [Ruby](https://www.ruby-lang.org/en/downloads/)
- [Java](https://www.java.com/en/download/)
- [Node and NPM](https://nodejs.org/en/)

## CLI
- [Yarn (NPM)](https://yarnpkg.com/lang/en/)
- [CyanPrint (Yarn)](https://yarnpkg.com/en/package/cyanprint)
- [Testcafe (Yarn)](https://yarnpkg.com/en/package/testcafe)
- [Vue cli (Yarn)](https://yarnpkg.com/en/package/@vue/cli)
- [Create React App (Yarn)](https://yarnpkg.com/en/package/create-react-app)
- [Netlify CLI (Yarn)](https://yarnpkg.com/en/package/netlify-deploy)
- [Netlify-deploy (Yarn)](https://www.nuget.org/packages/ImageDeployer/)
- [Webpack CLI (Yarn)](https://yarnpkg.com/en/package/webpack-cli)
- [Image Deploy (dotnet)]()
- [Anime Renamer (Gem)](https://rubygems.org/gems/anime_renamer)
- [PngQuant](https://pngquant.org/)
- [GraphicMagick](http://www.graphicsmagick.org/)
- [JpegOptim](https://github.com/tjko/jpegoptim)
- [FFMPEG](https://ffmpeg.org/)

## Development Tools
- [Visual Studio Code](https://code.visualstudio.com/)
- [Visual Studio 2019](https://visualstudio.microsoft.com/vs/preview/)
- [JetBrains Toolbox](https://www.jetbrains.com/toolbox/)
- [Notepad++](https://notepad-plus-plus.org/download/v7.6.4.html)
- [Git with LFS](https://git-scm.com/downloads)
- [Docker](https://www.docker.com/get-started)
- [Postman](https://www.getpostman.com/)
- [LaTeX Distrubiton](https://www.google.com/search?newwindow=1&source=hp&ei=ShucXJj8EMzhz7sPvMG3eA&q=latex+distribution&oq=latex+dis&gs_l=psy-ab.1.0.0l10.681.1875..2918...0.0..0.77.445.10......0....1..gws-wiz.....0..0i131.E1TlFMCd7aA)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [TCP View](https://docs.microsoft.com/en-gb/sysinternals/downloads/tcpview)

## Programs
- [GIMP - Photo Editor](https://www.gimp.org/)
- [BitTorrent - Torrent Client](https://www.bittorrent.com/)
- [Everything voidtools - Index searcher](https://www.voidtools.com/)
- [FxSound Enhancer -Equalizer](https://www.fxsound.com/)
- [ImageGlass - Image viewer](https://imageglass.org/release/imageglass-6-0-12-29-26)
- [pCloud - Storage Drive](https://my.pcloud.com/)
- [TryShift - Productivity tool](https://tryshift.com/)
- [Steam - Game client](https://store.steampowered.com/)
- [Windscribe - VPN](https://windscribe.com/)
- [PotPlayer - Video player](https://www.google.com.sg/search?q=potplayer&oq=potplayer&qs=chrome.0.0l6.1322j0j4&sourceid=chrome&ie=UTF-8)
- [Office 365](https://www.office.com/)
- [Rain Meter - Desktop Customization](https://www.rainmeter.net/)
- [Wallpaper engine - Desktop Customization](https://store.steampowered.com/app/431960/Wallpaper_Engine/)
- [B1 Zip - Archiving tool](https://b1.org/)

## Docker Services
- [PostgresSQL](https://www.postgresql.org/)
- [Redis](https://redis.io/)
